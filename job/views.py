from django.shortcuts import redirect, render
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from job.forms import ApplicantForm, JobForm, UserForm
from django.contrib.auth.decorators import login_required
from job.models import Job
from django.utils import timezone
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView

# Create your views here.

def index(request):
    return render(request, 'index.html')

@login_required
def special(request):
    return HttpResponse("You are logged in !")

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))

def register(request):
    registered = False
    if request.method == 'POST':
        print("post")
        user_form = UserForm(data = request.POST)
        if user_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user = user.save()
            return HttpResponseRedirect(reverse('index'))
        else:
            print(user_form.errors)
    else:
        user_form = UserForm()

    return render(request,'registration.html',{'user_form':user_form,'registered':registered})

def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username = username, password=password)
        
        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(reverse('index'))
            else:
                return HttpResponse("Your account was inactive.")
        else:
            print("Someone tried to login and failed.")
            print("They used username: {} and password:{}".format(username,password))
            return HttpResponse("Invalid login details given")
    else:
        return render(request,'login.html')

def job_view(request):
    job_list = Job.objects.all()
    return render(request,'job_list.html',{'jobs':job_list})


class JobDetailView(DetailView):

    model = Job
    template_name = 'job_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

class JobView(FormView):
    template_name = 'form.html'
    form_class = JobForm
    success_url = '/thanks/'

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        #form.send_email()
        return super().form_valid(form)

def applicant_form_upload(request):
    if request.method == 'POST':
        form = ApplicantForm(request.POST, request.FILES)
        if form.is_valid():
            # import pdb; pdb.set_trace()
            applicant_form = form.save(commit=False)
            applicant_form.user = request.user
            applicant_form.save()
        
            
            return redirect('index')
    else:
        form = ApplicantForm()
    return render(request, 'file_upload_form.html',{'form':form})
