from django.db import models
#from django.utils import timezone
from django.contrib.auth.models import User
from django.utils.text import slugify


JOB_TYPE = (
    ('Full time', "Full time"),
    ('Part time', "Part time"),
    ('Internship', "Internship"),
)

CHOICES = (
        ('Mumbai', 'Mumbai'),
        ('Delhi', 'Delhi'),
        ('Bangalore', 'Bangalore'),
        ('Hyderabad', 'Hyderabad'),
        ('Chennai', 'Chennai'),
        ('Jaipur', 'Jaipur'),
        ('Bhopal', 'Bhopal'),
        ('Patna', 'Patna'),

    )
class Job(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=300)
    skills = models.TextField()
    company = models.CharField(max_length=200)
    city = models.CharField(max_length=50, choices = CHOICES ,blank = True)
    description = models.TextField(blank = True)
    job_type = models.CharField(max_length=50, choices = JOB_TYPE ,blank = True)
    post_date = models.DateTimeField(auto_now_add = True, blank = True)
    about_job = models.TextField(blank = True)
    photo = models.FileField(blank = True)
    # slug = models.SlugField(unique = True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.company)
        super(Job, self).save(*args, **kwargs)
    
    def __str__(self):
        return self.title


class Applicant(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add = True)
    document = models.FileField(upload_to='media/files')

    def __str__(self):
        return self.user.email