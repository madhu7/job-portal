# Generated by Django 2.2.7 on 2019-11-28 06:19

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=300)),
                ('skills', models.TextField()),
                ('company', models.CharField(max_length=200)),
                ('city', models.CharField(blank=True, choices=[('Mumbai', 'Mumbai'), ('Delhi', 'Delhi'), ('Bangalore', 'Bangalore'), ('Hyderabad', 'Hyderabad'), ('Chennai', 'Chennai'), ('Jaipur', 'Jaipur'), ('Bhopal', 'Bhopal'), ('Patna', 'Patna')], max_length=50)),
                ('description', models.TextField(blank=True)),
                ('job_type', models.CharField(blank=True, choices=[('Full time', 'Full time'), ('Part time', 'Part time'), ('Internship', 'Internship')], max_length=50)),
                ('post_date', models.DateTimeField(auto_now_add=True)),
                ('about_job', models.TextField(blank=True)),
                ('photo', models.FileField(blank=True, upload_to='')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Applicant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('document', models.FileField(blank=True, upload_to='')),
                ('job', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='applicants', to='job.Job')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
