"""job_portal URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.urls import path
from job import views
from job.views import JobDetailView, JobView


urlpatterns = [
    path('admin/', admin.site.urls),
    path('register/',views.register,name='register'),
    path('',views.user_login, name = 'user_login'),
    path('logout/', views.user_logout , name = 'logout'),
    path('job_view/',views.job_view, name = 'job'),
    path('job/<int:pk>/', JobDetailView.as_view(), name='job_detail'),
    path('form/', JobView.as_view(), name = 'jobform'),
    path('index/',views.index, name ='index'),
    path('form_upload/', views.applicant_form_upload, name = 'form_upload')
]

if settings.DEBUG:
    urlpatterns= urlpatterns + static(settings.MEDIA_URL, document_root= settings.MEDIA_ROOT)
    urlpatterns= urlpatterns + static(settings.STATIC_URL, document_root= settings.STATIC_ROOT)