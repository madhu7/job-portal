from django import forms
from django.contrib.auth.models import User
from job.models import * 


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'email', 'password',]

class JobForm(forms.ModelForm):

    class Meta:
        model = Job
        exclude = ('slug',)
        
class ApplicantForm(forms.ModelForm):
    """Form definition for Applicant."""

    
    # def __init__(self, *args, **kwargs):
    #     self.user_id = kwargs.pop('user_id')
    #     super(ApplicantForm, self).__init__(*args, **kwargs)

    class Meta:
        """Meta definition for Applicantform."""

        model = Applicant
        fields = ('document',)
